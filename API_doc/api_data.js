define({ "api": [
  {
    "type": "post",
    "url": "/employees/create",
    "title": "Add an employee to the list.",
    "name": "CreateEmployee",
    "group": "Employees",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>First name of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>Last name of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>Three available options: 0 = N/A, 1 = M and 2 = F.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "birthdate",
            "description": "<p>Birthdate of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Current address of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.employeeID",
            "description": "<p>ID of the employee. The ID can be used to get access to the employee data.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.markerID",
            "description": "<p>ID of the marker. The ID can be used to get access to the location of the employee.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": \n    {\n        \"employeeID\": 4,\n        \"markerID\":   4\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 2,\n        \"message\": \"{Database error}\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Employees/Controller.js",
    "groupTitle": "Employees"
  },
  {
    "type": "delete",
    "url": "/employees/delete/:employeeID",
    "title": "Delete an employee and the marker of it.",
    "name": "DeleteEmployee",
    "group": "Employees",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if the employee is succesfully deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 8,\n        \"message\": \"The employee cannot be removed from the pack or has already been removed.\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Employees/Controller.js",
    "groupTitle": "Employees"
  },
  {
    "type": "get",
    "url": "/employees/get",
    "title": "Get all employees from the database.",
    "name": "GetEmployees",
    "group": "Employees",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.ID",
            "description": "<p>ID of the employee. The ID can be used to get access to the employee data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.firstName",
            "description": "<p>First name of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.lastName",
            "description": "<p>Last name of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.gender",
            "description": "<p>Three available options: 0 = N/A, 1 = M and 2 = F.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.birthdate",
            "description": "<p>Birthdate of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.markerID",
            "description": "<p>ID of the marker. The ID can be used to get access to the location of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.createdAt",
            "description": "<p>When the employee is made.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.updatedAt",
            "description": "<p>When the employee is updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.marker",
            "description": "<p>Information about the location.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.marker.address",
            "description": "<p>Address of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "data.marker.lat",
            "description": "<p>Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "data.marker.lng",
            "description": "<p>Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.marker.createdAt",
            "description": "<p>When the marker is made.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.marker.updatedAt",
            "description": "<p>When the marker is updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": \n    [\n        {\n            ID: 27,\n            firstName: \"Sander\",\n            lastName: \"Lem\",\n            gender: \"N/A\",\n            birthdate: \"2016-04-04\",\n            markerID: 23,\n            createdAt: \"2019-10-09T08:24:54.000Z\",\n            updatedAt: \"2019-10-09T08:24:54.000Z\",\n            marker: \n            {\n                address: \"Koeker 54, 5834AA\",\n                lat: -33.874737,\n                lng: 151.21553,\n                createdAt: \"2019-10-09T08:24:54.000Z\",\n                updatedAt: \"2019-10-09T08:24:54.000Z\"\n            }\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 4,\n        \"message\": \"Cannot find any employee.\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Employees/Controller.js",
    "groupTitle": "Employees"
  },
  {
    "type": "patch",
    "url": "/employees/update/:employeeID",
    "title": "Update data of the employee, including the marker.",
    "name": "UpdateEmployee",
    "group": "Employees",
    "version": "1.0.0",
    "description": "<p>Only the 'employeeID' is required and at least one of the other fields to update information.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "employeeID",
            "description": "<p>Employee ID must be passed as parameter.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>First name of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>Last name of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "gender",
            "description": "<p>Three available options: 0 = N/A, 1 = M and 2 = F.</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": false,
            "field": "birthdate",
            "description": "<p>Birthdate of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Current address of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lat",
            "description": "<p>Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          },
          {
            "group": "Parameter",
            "type": "Float",
            "optional": false,
            "field": "lng",
            "description": "<p>Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 2,\n        \"message\": \"{Database error}\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Employees/Controller.js",
    "groupTitle": "Employees"
  },
  {
    "type": "",
    "url": "Type",
    "title": "errors",
    "name": "Error_Codes",
    "group": "Error",
    "version": "1.0.0",
    "success": {
      "examples": [
        {
          "title": "HTTP status code 200:",
          "content": "error: { code: 0, message: \"Unknown error.\" }\nerror: { code: 1, message: \"Missing request parameters.\" }\nerror: { code: 2 }, // Database error\nerror: { code: 3, message: \"Cannot find the requested employee.\" }\nerror: { code: 4, message: \"Cannot find any employee.\" }\nerror: { code: 5, message: \"Cannot find any pack.\" }\nerror: { code: 6, message: \"The pack doesn't exist.\" }\nerror: { code: 7, message: \"A pack needs to have at least one employee.\" }\nerror: { code: 8, message: \"The employee cannot be removed from the pack or has already been removed.\" }\nerror: { code: 9, message: \"The employee is already added to the pack.\" }",
          "type": "json"
        },
        {
          "title": "HTTP status code 404:",
          "content": "error: \"404 Not Found\"",
          "type": "json"
        },
        {
          "title": "HTTP status code 429:",
          "content": "error: \"Too many request from this IP, please try again after a half hour.\"",
          "type": "json"
        },
        {
          "title": "HTTP status code 500:",
          "content": "error: { error: {empty in production environment}, message  },",
          "type": "json"
        }
      ]
    },
    "filename": "API/Services/ErrorTypes.js",
    "groupTitle": "Error"
  },
  {
    "type": "patch",
    "url": "/packs/add/employee/:packID/:employeeID",
    "title": "Add an employee to a pack.",
    "name": "AddEmployeeToPack",
    "group": "Packs",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "packID",
            "description": "<p>ID of the pack.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "employeeID",
            "description": "<p>ID of the employee.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data",
            "description": "<p>ID of the employee pack. The ID is used to delete an employee from a pack.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"success\": true,\n     \"data\": 16\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 9,\n        \"message\": \"The employee is already added to the pack.\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "API/Components/Packs/Controller.js",
    "groupTitle": "Packs"
  },
  {
    "type": "post",
    "url": "/packs/create",
    "title": "Create a new pack with at least one employee.",
    "name": "CreatePack",
    "group": "Packs",
    "description": "<p>A pack consist out of one employee at least, so it is required to pass an employee ID.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "employeeID",
            "description": "<p>ID of the employee.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "packName",
            "description": "<p>The name of the pack.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.packID",
            "description": "<p>ID of the pack.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.employeePackID",
            "description": "<p>ID of the employee pack. You can get the employee ID and pack ID with the 'employeePackID'. The ID is needed to delete an employee from a pack.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n     \"success\": true,\n     \"data\": \n     {\n        \"packID\": 4,\n        \"employeePackID\":   20\n     }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 2,\n        \"message\": \"{Database error}\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "API/Components/Packs/Controller.js",
    "groupTitle": "Packs"
  },
  {
    "type": "delete",
    "url": "/packs/delete/employee/:packID/:employeePackID",
    "title": "Delete an employee from a pack.",
    "name": "DeleteEmployeeFromPack",
    "group": "Packs",
    "version": "1.0.0",
    "description": "<p>A pack has to consist out of one employee at least.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "packID",
            "description": "<p>ID of the pack.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "employeePackID",
            "description": "<p>ID of the employee pack.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if the employee is succesfully deleted.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 6,\n        \"message\": \"The pack doesn't exist.\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Packs/Controller.js",
    "groupTitle": "Packs"
  },
  {
    "type": "get",
    "url": "/packs/get",
    "title": "Get all packs with the employees data.",
    "name": "GetPacks",
    "group": "Packs",
    "version": "1.0.0",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>True if everything happened succesfully, otherwise false.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.ID",
            "description": "<p>ID of the pack.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.name",
            "description": "<p>Name of the pack.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.createdAt",
            "description": "<p>When the pack is made.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.updatedAt",
            "description": "<p>When the pack is updated.</p>"
          },
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data.employeePacks",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.employeePacks.ID",
            "description": "<p>ID of the employee pack.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "data.employeePacks.employee",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "datadata.employeePacks.employee.ID",
            "description": "<p>ID of the employee. The ID can be used to get access to the employee data.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "datadata.employeePacks.employee.firstName",
            "description": "<p>First name of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.employeePacks.employee.lastName",
            "description": "<p>Last name of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "data.employeePacks.employee.gender",
            "description": "<p>Three available options: 0 = N/A, 1 = M and 2 = F.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "data.employeePacks.employee.birthdate",
            "description": "<p>Birthdate of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "data.employeePacks.employee.markerID",
            "description": "<p>ID of the marker. The ID can be used to get access to the location of the employee.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.employeePacks.employee.createdAt",
            "description": "<p>When the employee is made.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "data.employeePacks.employee.updatedAt",
            "description": "<p>When the employee is updated.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": true,\n    \"data\": \n    [\n        {\n             ID: 12,\n             name: \"Sander Pack\",\n             createdAt: \"2019-10-09T20:17:18.000Z\",\n             updatedAt: \"2019-10-09T20:17:18.000Z\",\n             employeePacks: \n             [\n                 {\n                     ID: 19,\n                     employee: \n                     {\n                         ID: 27,\n                         firstName: \"Sander\",\n                         lastName: \"Lem\",\n                         gender: \"N/A\",\n                         birthdate: \"2016-04-04\",\n                         markerID: 23,\n                         createdAt: \"2019-10-09T08:24:54.000Z\",\n                         updatedAt: \"2019-10-09T08:24:54.000Z\"\n                     }\n                 }\n             ]\n         }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Bool",
            "optional": false,
            "field": "success",
            "description": "<p>False because something went wrong.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Object",
            "optional": false,
            "field": "error",
            "description": ""
          },
          {
            "group": "Error 4xx",
            "type": "Number",
            "optional": false,
            "field": "error.code",
            "description": "<p>The error code.</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "error.message",
            "description": "<p>The reason why it went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 200 OK\n{\n    \"success\": false,\n    \"error\": \n    {\n        \"code\": 5,\n        \"message\": \"Cannot find any pack.\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "API/Components/Packs/Controller.js",
    "groupTitle": "Packs"
  }
] });
