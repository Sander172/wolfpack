The config folder should contain a file with the environment variables. 

To get the project running:
1. Add a file named: 'Development.env'.
2. Copy and paste the settings I've sent through the mail.

Environment variables that are required:

    - ENVIRONMENT

    - PORT
    - HOST
    - PROTOCOL

    - DB_HOST
    - DB_USER
    - DB_PASS
    - DB_NAME