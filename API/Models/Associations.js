module.exports = function(models)
{
    models['employees'].hasOne(models['markers'], {  foreignKey: 'ID', sourceKey: 'markerID' });
    models['packs'].hasMany(models['employeePacks'], {  foreignKey: 'packID', sourceKey: 'ID' });
    models['employees'].hasMany(models['employeePacks'], {  foreignKey: 'employeeID', sourceKey: 'ID' });
    models['employeePacks'].belongsTo(models['employees']);
}