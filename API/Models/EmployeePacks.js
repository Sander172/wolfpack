module.exports  = function(sequelize, Sequelize)
{
    const employeePacks = sequelize.define('employeePacks', {
        ID: 
        {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        employeeID: 
        {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        packID: 
        {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    }, 
    {
        timestamps: false
    });

    return employeePacks;
}