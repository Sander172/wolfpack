module.exports  = function(sequelize, Sequelize)
{
    const employees = sequelize.define('employees', {
        ID: 
        {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        firstName: 
        {
            type: Sequelize.STRING,
            allowNull: false,
            validate: 
            {
                is: ["^[a-z]+$",'i'],    // Will only allow letters      
                notEmpty: true,
                firstLetterToUppercase(value) 
                {
                    value = value.charAt(0).toUpperCase() + value.slice(1);
                }
            }
        },
        lastName: 
        {
            type: Sequelize.STRING,
            allowNull: false,
            validate: 
            {
                is: ["^[a-z]+$",'i'],    // Will only allow letters      
                notEmpty: true,
                firstLetterToUppercase(value) 
                {
                    value = value.charAt(0).toUpperCase() + value.slice(1);
                }
            }
        },
        gender: 
        {
            type: Sequelize.ENUM('N/A', 'M', 'F'),
            allowNull: false
        },
        birthdate: 
        {
            type: Sequelize.DATEONLY,
            allowNull: false
        },
        markerID: 
        {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return employees;
}