module.exports  = function(sequelize, Sequelize)
{
    const markers = sequelize.define('markers', {
        address: 
        {
            type: Sequelize.STRING,
            allowNull: false,
            validate: 
            {
                notEmpty: true,
                firstLetterToUppercase(value) 
                {
                    value = value.charAt(0).toUpperCase() + value.slice(1);
                }
            }
        },
        lat: 
        {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        lng: 
        {
            type: Sequelize.FLOAT,
            allowNull: false
        }
    });

    return markers;
}