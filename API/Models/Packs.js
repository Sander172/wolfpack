module.exports  = function(sequelize, Sequelize)
{
    const packs = sequelize.define('packs', {
        ID: 
        {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: 
        {
            type: Sequelize.STRING,
            allowNull: false,
            validate: 
            {
                notEmpty: true,
                firstLetterToUppercase(value) 
                {
                    value = value.charAt(0).toUpperCase() + value.slice(1);
                }
            }
        }
    });

    return packs;
}