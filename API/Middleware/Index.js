const app               = require('../../App');
const rateLimitSettings = require('../Services/RateLimitSettings');

const Logger = (req, res, next) =>
{
    logger.info(`Logged  ${req.url} - ${req.method} - ${new Date()}`);
    next();
}

// Add logger middleware
app.use(Logger);

// Add rate limit
app.use("/api/", rateLimitSettings.apiLimiter);