const app = require('../App');
// Components
const employees = require('./Components/Employees/Routes');
const packs = require('./Components/Packs/Routes');

employees(app);
packs(app);