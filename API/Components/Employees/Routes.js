const express           = require('express');
const route             = express.Router();
const rateLimitSettings = require('../../Services/RateLimitSettings');
// Get the employees controller
const controller    = require('./Controller');

module.exports = function(app)
{
    app.use('/api/employees', route);

    // Get employees
    route.get('/get', async function (req, res) 
    {
        await controller.GetEmployees(req, res);
    });

    // Create an employee
    route.post('/create', rateLimitSettings.createLimiter, async function (req, res) 
    {
        await controller.CreateEmployee(req, res);
    });

    // Create an employee
    route.patch('/update/:employeeID', async function (req, res) 
    {
        await controller.UpdateEmployee(req, res);
    });

    // Delete an employee
    route.delete('/delete/:employeeID', async function (req, res) 
    {
        await controller.DeleteEmployee(req, res);
    });
}