const database      = require('../../Services/Database');
const errorTypes    = require('../../Services/ErrorTypes');
const validation    = require('../../Services/Validation');
const moment        = require('moment');

module.exports  = 
{
    /**
      * @api {get} /employees/get Get all employees from the database.
      * @apiName GetEmployees
      * @apiGroup Employees
      * @apiVersion 1.0.0
      *       
      * @apiSuccess {Bool} success                      True if everything happened succesfully, otherwise false.
      * @apiSuccess {Array} data 
      * @apiSuccess {Number} data.ID                    ID of the employee. The ID can be used to get access to the employee data.
      * @apiSuccess {String} data.firstName             First name of the employee.
      * @apiSuccess {String} data.lastName              Last name of the employee.
      * @apiSuccess {String} data.gender                Three available options: 0 = N/A, 1 = M and 2 = F.
      * @apiSuccess {Date} data.birthdate               Birthdate of the employee.
      * @apiSuccess {Number} data.markerID              ID of the marker. The ID can be used to get access to the location of the employee.
      * @apiSuccess {Datetime} data.createdAt           When the employee is made.
      * @apiSuccess {Datetime} data.updatedAt           When the employee is updated.
      * @apiSuccess {Object} data.marker                Information about the location.
      * @apiSuccess {String} data.marker.address        Address of the employee.
      * @apiSuccess {Float} data.marker.lat             Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      * @apiSuccess {Float} data.marker.lng             Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      * @apiSuccess {Datetime} data.marker.createdAt    When the marker is made.
      * @apiSuccess {Datetime} data.marker.updatedAt    When the marker is updated.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true,
      *         "data": 
      *         [
      *             {
      *                 ID: 27,
      *                 firstName: "Sander",
      *                 lastName: "Lem",
      *                 gender: "N/A",
      *                 birthdate: "2016-04-04",
      *                 markerID: 23,
      *                 createdAt: "2019-10-09T08:24:54.000Z",
      *                 updatedAt: "2019-10-09T08:24:54.000Z",
      *                 marker: 
      *                 {
      *                     address: "Koeker 54, 5834AA",
      *                     lat: -33.874737,
      *                     lng: 151.21553,
      *                     createdAt: "2019-10-09T08:24:54.000Z",
      *                     updatedAt: "2019-10-09T08:24:54.000Z"
      *                 }
      *             }
      *         ]
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {String} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 4,
      *             "message": "Cannot find any employee."
      *         }
      *     }
      */
    GetEmployees: async function(req, res)
    {
        const employees = await database.models['employees'].findAll({
            include: 
            [{
                model: database.models['markers'],
                attributes: { exclude: ['ID', 'id'] },
                required: true
            }]
        });

        if (employees.length == 0) 
        {
            return res.json({ success: false, error: errorTypes.errors[4] });
        } 
        
        return res.json({ success: true, data: employees });
    },
    /**
     * @description Get a specific employee from the database.
     * @returns 'data' an object with all the employees info.
     *          'error' error message when success is false.
     *          'success' true if the request succeeded, otherwise false.
     */
    GetEmployee: async function(employeeID)
    {
        const employee = await database.models['employees'].findOne({ 
            where: { ID: employeeID },
            include: 
            [{
                model: database.models['markers'],
                attributes: { exclude: ['ID', 'id'] },
                required: true
            }]
        });

        if (employee === null)
        {
            return { success: false, error: errorTypes.errors[3] };
        }
        
        return { success: true, data: employee };
    },
    /**
      * @api {post} /employees/create Add an employee to the list.
      * @apiName CreateEmployee
      * @apiGroup Employees
      * @apiVersion 1.0.0
      * 
      * @apiParam {String} firstName            First name of the employee.
      * @apiParam {String} lastName             Last name of the employee.
      * @apiParam {Number} gender               Three available options: 0 = N/A, 1 = M and 2 = F.
      * @apiParam {Date} birthdate              Birthdate of the employee.
      * @apiParam {String} address              Current address of the employee.
      * @apiParam {Float} lat                   Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      * @apiParam {Float} lng                   Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      
      * @apiSuccess {Bool} success              True if everything happened succesfully, otherwise false.
      * @apiSuccess {Object} data 
      * @apiSuccess {Number} data.employeeID    ID of the employee. The ID can be used to get access to the employee data.
      * @apiSuccess {Number} data.markerID      ID of the marker. The ID can be used to get access to the location of the employee.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true,
      *         "data": 
      *         {
      *             "employeeID": 4,
      *             "markerID":   4
      *         }
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {String} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 2,
      *             "message": "{Database error}"
      *         }
      *     }
      */
    CreateEmployee: async function(req, res)
    { 
        const paramsResult = validation.CheckParameters(["firstName", "lastName", "gender", "birthdate", "address", "lat", "lng"], req.body);
       
        if (paramsResult.success === false) 
        { 
            return paramsResult;
        }

        // Add a marker to the database
        const marker = await database.models['markers'].create({ 
            'address' : req.body.address,
            'lat' : req.body.lat,
            'lng' : req.body.lng,
        }).catch(err => {
            logger.error('Unable to create a marker:', err); 
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });
    
        // Add an employee to the database
        const employee = await database.models['employees'].create({ 
            'firstName' : req.body.firstName,
            'lastName'  : req.body.lastName,
            'gender'    : req.body.gender,
            'birthdate' : req.body.birthdate,
            'markerID'  : marker.dataValues.id
        }).catch(err => {
            logger.error('Unable to create an employee:', err); 
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        }); 
    
        return res.json({ success: true, data: { employeeID: employee.ID, markerID: marker.dataValues.id } }); 
    },
    /**
      * @api {patch} /employees/update/:employeeID Update data of the employee, including the marker.
      * @apiName UpdateEmployee
      * @apiGroup Employees
      * @apiVersion 1.0.0
      * @apiDescription Only the 'employeeID' is required and at least one of the other fields to update information. 
      * 
      * @apiParam {Number} employeeID           Employee ID must be passed as parameter. 
      * @apiParam {String} firstName            First name of the employee.
      * @apiParam {String} lastName             Last name of the employee.
      * @apiParam {Number} gender               Three available options: 0 = N/A, 1 = M and 2 = F.
      * @apiParam {Date} birthdate              Birthdate of the employee.
      * @apiParam {String} address              Current address of the employee.
      * @apiParam {Float} lat                   Latitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      * @apiParam {Float} lng                   Longitude represent the coordinates at geographic coordinate system. Between 6 - 10 numbers.
      
      * @apiSuccess {Bool} success              True if everything happened succesfully, otherwise false.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {String} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 2,
      *             "message": "{Database error}"
      *         }
      *     }
      */
    UpdateEmployee: async function(req, res)
    {
        validation.RemoveUnpassedParams(["firstName", "lastName", "gender", "birthdate", "address", "lat", "lng"], req.body);

        const employee = await module.exports.GetEmployee(req.params["employeeID"]);

        if (employee.success === false) 
        {
            return res.json(employee);
        }

        // Add an employee to the database
        const employeeUpdated = await database.models['employees'].update(
            req.body,
            {
                where: { ID: req.params['employeeID'] }
            }
        ).catch(err => {
            logger.error('Unable to create an employee:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });
    
        // Add a marker to the database
        const marker = await database.models['markers'].update(
            req.body,
            {
                where: { ID: employee.data.markerID }
            }
        ).catch(err => {
            logger.error('Unable to create a marker:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        }); 

        return res.json({ success: true });
    },
    /**
      * @api {delete} /employees/delete/:employeeID Delete an employee and the marker of it.
      * @apiName DeleteEmployee
      * @apiGroup Employees
      * @apiVersion 1.0.0
      * 
      * @apiSuccess {Bool} success              True if the employee is succesfully deleted.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {Number} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 8,
      *             "message": "The employee cannot be removed from the pack or has already been removed."
      *         }
      *     }
      */
    DeleteEmployee: async function(req, res)
    {
        await sequelize.query('DELETE employees, markers ' +
                        'FROM employees ' +
                        'INNER JOIN markers ON employees.markerID = markers.ID ' +
                        'WHERE employees.ID = ' + req.params['employeeID']).then(data => {
            // There is nothing deleted
            if (data[0].affectedRows == 0)
            {
                return res.json({ success: false, error: errorTypes.errors[8] });
            }             
        }).catch(err => {
            logger.error('Unable to delete an employee:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        }); 

        return res.json({ success: true }); 
    }
};