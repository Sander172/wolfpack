const database      = require('../../Services/Database');
const errorTypes    = require('../../Services/ErrorTypes');
const validation    = require('../../Services/Validation');

module.exports  = 
{ 
    /**
      * @api {get} /packs/get Get all packs with the employees data.
      * @apiName GetPacks
      * @apiGroup Packs
      * @apiVersion 1.0.0
      *       
      * @apiSuccess {Bool} success                                              True if everything happened succesfully, otherwise false.
      * @apiSuccess {Array} data 
      * @apiSuccess {Number} data.ID                                            ID of the pack. 
      * @apiSuccess {String} data.name                                          Name of the pack.
      * @apiSuccess {Datetime} data.createdAt                                   When the pack is made.
      * @apiSuccess {Datetime} data.updatedAt                                   When the pack is updated.
      * @apiSuccess {Array} data.employeePacks         
      * @apiSuccess {Number} data.employeePacks.ID                              ID of the employee pack.
      * @apiSuccess {Object} data.employeePacks.employee        
      * @apiSuccess {Number} datadata.employeePacks.employee.ID                 ID of the employee. The ID can be used to get access to the employee data.
      * @apiSuccess {String} datadata.employeePacks.employee.firstName          First name of the employee.  
      * @apiSuccess {String} data.employeePacks.employee.lastName               Last name of the employee.
      * @apiSuccess {String} data.employeePacks.employee.gender                 Three available options: 0 = N/A, 1 = M and 2 = F.
      * @apiSuccess {Date} data.employeePacks.employee.birthdate                Birthdate of the employee.
      * @apiSuccess {Number} data.employeePacks.employee.markerID               ID of the marker. The ID can be used to get access to the location of the employee.
      * @apiSuccess {Datetime} data.employeePacks.employee.createdAt            When the employee is made.
      * @apiSuccess {Datetime} data.employeePacks.employee.updatedAt            When the employee is updated.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true,
      *         "data": 
      *         [
      *             {
      *                  ID: 12,
      *                  name: "Sander Pack",
      *                  createdAt: "2019-10-09T20:17:18.000Z",
      *                  updatedAt: "2019-10-09T20:17:18.000Z",
      *                  employeePacks: 
      *                  [
      *                      {
      *                          ID: 19,
      *                          employee: 
      *                          {
      *                              ID: 27,
      *                              firstName: "Sander",
      *                              lastName: "Lem",
      *                              gender: "N/A",
      *                              birthdate: "2016-04-04",
      *                              markerID: 23,
      *                              createdAt: "2019-10-09T08:24:54.000Z",
      *                              updatedAt: "2019-10-09T08:24:54.000Z"
      *                          }
      *                      }
      *                  ]
      *              }
      *         ]
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {String} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 5,
      *             "message": "Cannot find any pack."
      *         }
      *     }
      */
    GetPacks: async function(req, res)
    {
        const packs = await database.models['packs'].findAll({
            include: 
            [
                {
                    model: database.models['employeePacks'],
                    attributes: { exclude: ['employeeID', 'packID'] },
                    required: true,
                    include: 
                    [
                        {
                            model: database.models['employees'],
                            required: true
                        }
                    ]
                }
            ]
        });

        if (packs.length == 0) 
        {
            return res.json({ success: false, error: errorTypes.errors[5] });
        } 
        
        return res.json({ success: true, data: packs });
    },
    /**
     * @api {post} /packs/create Create a new pack with at least one employee.
     * @apiName CreatePack
     * @apiGroup Packs
     * @apiDescription A pack consist out of one employee at least, so it is required to pass an employee ID.
     * 
     * @apiParam {Number} employeeID            ID of the employee.
     * @apiParam {String} packName              The name of the pack.
     *
     * @apiSuccess {Bool} success               True if everything happened succesfully, otherwise false.
     * @apiSuccess {Object} data 
     * @apiSuccess {Number} data.packID         ID of the pack.
     * @apiSuccess {Number} data.employeePackID ID of the employee pack. You can get the employee ID and pack ID with the 'employeePackID'. The ID is needed to delete an employee from a pack.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "success": true,
     *          "data": 
     *          {
     *             "packID": 4,
     *             "employeePackID":   20
     *          }
     *     }
     *
     * @apiError {Bool} success                False because something went wrong.
     * @apiError {Object} error 
     * @apiError {Number} error.code           The error code.
     * @apiError {String} error.message        The reason why it went wrong.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 200 OK
     *     {
     *         "success": false,
     *         "error": 
     *         {
     *             "code": 2,
     *             "message": "{Database error}"
     *         }
     *     }
     */
    CreatePack: async function(req, res)
    {
        const paramsResult = validation.CheckParameters(["packName", "employeeID"], req.body);
        
        if (paramsResult.success === false) 
        {
            return paramsResult;
        }

        // Add a pack to the database
        const pack = await database.models['packs'].create({ 'name' : req.body.packName }).catch(err => {
            logger.error('Unable to create a pack:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });
     
        const employeePack = await database.models['employeePacks'].create({ 'employeeID' : req.body.employeeID, 'packID' : pack.ID }).catch(err => {
            logger.error('Unable to add an employee to a pack:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });

        return res.json({ success: true, data: { packID: pack.ID, employeePackID: employeePack.ID } });
    },
    /**
     * @api {patch} /packs/add/employee/:packID/:employeeID Add an employee to a pack.
     * @apiName AddEmployeeToPack
     * @apiGroup Packs
     * 
     * @apiParam {Number} packID                ID of the pack.
     * @apiParam {Number} employeeID            ID of the employee.
     *
     * @apiSuccess {Bool} success               True if everything happened succesfully, otherwise false.
     * @apiSuccess {Number} data                ID of the employee pack. The ID is used to delete an employee from a pack.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *          "success": true,
     *          "data": 16
     *     }
     *
     * @apiError {Bool} success                False because something went wrong.
     * @apiError {Object} error 
     * @apiError {Number} error.code           The error code.
     * @apiError {String} error.message        The reason why it went wrong.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 200 OK
     *     {
     *         "success": false,
     *         "error": 
     *         {
     *             "code": 9,
     *             "message": "The employee is already added to the pack."
     *         }
     *     }
     */
    AddEmployeePack: async function(req, res)
    {
        const totalEmployees = await database.models['employeePacks'].count({ where: { employeeID: req.params['employeeID'], packID: req.params['packID'] } });

        // The employee is already added to the pack
        if (totalEmployees > 0)
        {
            return res.json({ success: false, error: errorTypes.errors[9] });
        }

        const employeePack = await database.models['employeePacks'].create({ employeeID: req.params['employeeID'], packID: req.params['packID'] }).catch(err => {
            logger.error('Unable to add an employee to a pack:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });

        return res.json({ success: true, data: employeePack.ID });
    },
    /**
      * @api {delete} /packs/delete/employee/:packID/:employeePackID Delete an employee from a pack.
      * @apiName DeleteEmployeeFromPack
      * @apiGroup Packs
      * @apiVersion 1.0.0
      * @apiDescription A pack has to consist out of one employee at least.
      * 
      * @apiParam {Number} packID           ID of the pack.
      * @apiParam {Number} employeePackID   ID of the employee pack.
      * 
      * @apiSuccess {Bool} success              True if the employee is succesfully deleted.
      *
      * @apiSuccessExample Success-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": true
      *     }
      *
      * @apiError {Bool} success                False because something went wrong.
      * @apiError {Object} error 
      * @apiError {Number} error.code           The error code.
      * @apiError {String} error.message        The reason why it went wrong.
      *
      * @apiErrorExample Error-Response:
      *     HTTP/1.1 200 OK
      *     {
      *         "success": false,
      *         "error": 
      *         {
      *             "code": 6,
      *             "message": "The pack doesn't exist."
      *         }
      *     }
      */
    DeleteEmployeeFromPack: async function(req, res)
    {
        const totalEmployees = await database.models['employeePacks'].count({ where: { packID: req.params['packID'] } });

        // A pack needs to have at least one employee
        if (totalEmployees <= 1)
        {
            return res.json({ success: false, error: errorTypes.errors[(6 + totalEmployees)] });
        }

        // Remove the employee from the pack
        const employeePackRemoved = await database.models['employeePacks'].destroy({ where: { ID: req.params["employeePackID"] } }).catch(err => {
            logger.error('Unable to delete an employee from a pack:', err);
            return res.json({ success: false, error: { code: errorTypes.errors[2].code, message: err.name } });
        });

        if (employeePackRemoved == 0)
        {
            return res.json({ success: false, error: errorTypes.errors[8] });
        }

        return res.json({ success: true });
    }
};