const express           = require('express');
const route             = express.Router();
const rateLimitSettings = require('../../Services/RateLimitSettings');
// Get the employees controller
const controller        = require('./Controller');

module.exports = function(app)
{
    app.use('/api/packs', route);

    // Get packs
    route.get('/get', async function (req, res) 
    {
        await controller.GetPacks(req, res);
    });

    // Create a pack with at least one employee
    route.post('/create', rateLimitSettings.createLimiter, async function (req, res) 
    {
        await controller.CreatePack(req, res);
    });

    // Add an employee to a pack
    route.patch('/add/employee/:packID/:employeeID', async function (req, res) 
    {
        await controller.AddEmployeePack(req, res);
    });

    // Delete an employee from a pack
    route.delete('/delete/employee/:packID/:employeePackID', async function (req, res) 
    {
        await controller.DeleteEmployeeFromPack(req, res);
    });
}