const app       = require('../../App');
const fs        = require('fs');
const path      = require('path');
const Sequelize = require('sequelize');

sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, 
{
    host: process.env.DB_HOST,
    logging: false,
    dialect: 'mysql',
    pool: 
    {
        max: 25,
        min: 0,
        idle: 10000
    }
});

sequelize
    .authenticate()
    .then(() => 
    {
        logger.info('Connection with the database has been established.');

        // Load all the models
        LoadModels();
    })
    .catch(err => 
    {
        logger.error('Unable to connect to the database:', err);
        process.exit(1);
    });

// Object with all the models
module.exports.models = {};

/**
 * description Load all models that are in the API/Models folder. Those models will be set in the global 'models' object,
 * which can be used in the controllers, et cetera.
 */
function LoadModels()
{
    const modelPath = __dirname + '/../Models';
    const modelFiles = fs.readdirSync(modelPath).filter(function() { return true; });
    
    for(var i = 0; i < modelFiles.length; ++i) 
    {
        if (modelFiles[i] === "Associations.js") continue;
    
        const model = require(path.join(modelPath, modelFiles[i]))(sequelize, Sequelize);
        module.exports.models[model.name] = model;
    }   
    
    // Setup associations
    require("../Models/Associations")(module.exports.models);
}