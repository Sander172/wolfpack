const app = require('../../App');

// Custom 404 page
app.use(function (req, res, next) {
    res.status(404);

    // Respond with html page
    if (req.accepts('html')) 
    {
        res.send('404 Not Found');
        return;
    }

    // Respond with json
    if (req.accepts('json')) 
    {
        res.send({ error: '404 Not found' });
        return;
    }

    // Default to plain-text
    res.type('txt').send('404 Not found');
});

if (process.env.ENVIRONMENT === 'development') 
{
    app.use(function(err, req, res, next) 
    {
        logger.error(err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// No stacktraces leaked to the user
if (process.env.ENVIRONMENT === 'production') 
{
    app.use(function(err, req, res, next) 
    {
        logger.error(err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: {}
        });
    });
}