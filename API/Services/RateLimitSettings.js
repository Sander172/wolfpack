const rateLimit     = require("express-rate-limit");
const errorTypes    = require('./ErrorTypes');

module.exports.apiLimiter = rateLimit({
    windowMs: 30 * 60 * 1000, // 30 minutes
    max: 150, 
    onLimitReached:
        function (req, res, options) 
        {
            res.status(429).send({ error: errorTypes.errors[10].message });
        }
});

module.exports.createLimiter = rateLimit({
    windowMs: 30 * 60 * 1000, // 30 minutes
    max: 30, 
    onLimitReached:
        function (req, res, options) 
        {
            res.status(429).send({ error: errorTypes.errors[10].message });
        }
});