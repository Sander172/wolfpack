const errorTypes = require('./ErrorTypes');

module.exports = 
{
    /**
     * description     Check if all fields are sent with the request.
     * param {Array}   requiredFields      Array (string) with all the required fields. 
     * param {Object}  data                Info inside the request body, session or a random object that is passed as reference.
     * param {Bool}    removeOtherFields   Delete information from data. Note: object is passed as reference, so it will remove data from the request.body, or whatever
     *                                      has been passed.
     * returns {JSON}  'success' true if all fields have been sent, otherwise false.
     *                  'error' Will only be sent when 'success' is false. 'error' is an object with the error 'code', 'message' and 'details' which 
     *                  contains the fields that are not passed.
     **/
    CheckParameters: function(requiredFields, data, removeOtherFields = false)
    {
        var hasKey = false;
        var fieldsFailed = [];

        requiredFields.forEach(function (requiredKey) 
        {
            for (key in data)
            {
                if (requiredKey == key) 
                {
                    hasKey = true;
                }
            }

            if (!hasKey) {  
                fieldsFailed.push(requiredKey);
            }
            else {
                hasKey = false;
            }
        });

        // Remove fields from the 'data' array. Note: it removes data from the request.body, request.session or whatever has been passed.
        if (removeOtherFields) 
        {
            module.exports.RemoveUnpassedParams(requiredFields, data);
        }

        // Missing parameters
        if (fieldsFailed.length > 0 || (Object.getOwnPropertyNames(data).length === 0)) 
        {
            const response =  { success: false, error: { details: fieldsFailed } };
            Object.assign(response.error, errorTypes.errors[1]);

            return response;
        }

        return { success: true };
    },
    /**
     * description     Remove all fields from the 'data' that are not specified in the 'acceptableFields' array.
     * param {Array}   acceptableFields    Array (string) with all the fields that are accepted. 
     * param {Object}  data                Info inside the request body, session or a random object that is passed as reference.
     **/
    RemoveUnpassedParams: function(acceptableFields, data)
    {
        var hasKey = false;
        
        for (key in data)
        {
            acceptableFields.forEach(function (requiredKey) 
            {
                if (key == requiredKey) 
                {
                    hasKey = true;
                }
            });

            if (!hasKey) 
            {
                delete data[key];
                continue;
            }

            hasKey = false;
        }

        return;
    }
}