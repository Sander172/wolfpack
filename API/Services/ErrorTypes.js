const app = require('../../App');

/**
 * @api Type errors
 * @apiName Error Codes
 * @apiGroup Error
 * @apiVersion 1.0.0
 * 
 * @apiSuccessExample HTTP status code 200:
 *      error: { code: 0, message: "Unknown error." }
 *      error: { code: 1, message: "Missing request parameters." }
 *      error: { code: 2 }, // Database error
 *      error: { code: 3, message: "Cannot find the requested employee." }
 *      error: { code: 4, message: "Cannot find any employee." }
 *      error: { code: 5, message: "Cannot find any pack." }
 *      error: { code: 6, message: "The pack doesn't exist." }
 *      error: { code: 7, message: "A pack needs to have at least one employee." }
 *      error: { code: 8, message: "The employee cannot be removed from the pack or has already been removed." }
 *      error: { code: 9, message: "The employee is already added to the pack." }
 * 
 * 
 * @apiSuccessExample HTTP status code 404:
 *      error: "404 Not Found"
 * 
 * @apiSuccessExample HTTP status code 429:
 *      error: "Too many request from this IP, please try again after a half hour."
 * 
 * @apiSuccessExample HTTP status code 500:
 *      error: { error: {empty in production environment}, message  },
 *      
 */
const errors = 
[
    { code: 0, message: "Unknown error." },
    { code: 1, message: "Missing request parameters." },
    { code: 2 }, // Database error
    { code: 3, message: "Cannot find the requested employee." },
    { code: 4, message: "Cannot find any employee." },
    { code: 5, message: "Cannot find any pack." },
    { code: 6, message: "The pack doesn't exist." },
    { code: 7, message: "A pack needs to have at least one employee." },
    { code: 8, message: "The employee cannot be removed from the pack or has already been removed." },
    { code: 9, message: "The employee is already added to the pack." },
    { message: "Too many request from this IP, please try again after a half hour." }
];

module.exports.errors = errors;