const assert = require('assert');

module.exports = 
{
    PromiseTo: function(promise) 
    {
        assert(promise instanceof Promise, "Catch handler is called, but no promise has been passed!");

        return promise.then(data => {
            return [null, data];
        })
        .catch(err => [err]);
    }
}