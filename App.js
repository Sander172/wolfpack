/// Modules
const express               = require('express');
const app                   = module.exports = express();
// Module for loading environments
const dotenv                = require('dotenv');
// Helps you secure your Express app by setting various HTTP headers
const helmet = require('helmet')
// Logger module
const winston               = require('winston');
const consoleTransport      = new winston.transports.Console();
const myWinstonOptions      = 
{
    transports: [consoleTransport]
};
logger                      = new winston.createLogger(myWinstonOptions); 
// Body parser
const bodyParser            = require('body-parser');

/// Initialize middleware
require('./API/Middleware/Index');

/// Load development environments
dotenv.config({ path: 'Config/Development.env' });

/// Initialize the database
require('./API/Services/Database');

/// Initialize Helmet
app.use(helmet());

/// Initialize bodyParser
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: false }));

/// Initialize the API routes
require('./API/Routes');

/// Initialize error status 404 and 500
require('./API/Services/ErrorStatus');

/// Start the server
app.listen(process.env.PORT, function () {
    logger.info(`Server is listening at ${process.env.PROTOCOL}://${process.env.HOST}:${process.env.PORT}`);
});