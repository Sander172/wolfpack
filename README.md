Wolfpack

How to run the project

    1. Download the project.

    2. Install node modules.

    3. Add a 'Development.env' script to the 'Config' folder.

    4. Run the project: 'npm run devStart' or 'npm run start'.